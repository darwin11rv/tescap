# stat-alf
aplicación de test con capacitor y ionic
# Se listan los pasos para usar capacitor
# Entorno

Angular CLI: 8.1.3
Node: 12.15.0
OS: linux x64
Angular: 8.1.3
... cli, common, compiler, compiler-cli, core, forms
... language-service, platform-browser, platform-browser-dynamic
... router

| Package | Version |
| :------- | :------- |
| @angular-devkit/architect | 0.801.3 |
| @angular-devkit/build-angular | 0.801.3 |
| @angular-devkit/build-optimizer | 0.801.3 |
| @angular-devkit/build-webpack | 0.801.3 |
| @angular-devkit/core | 8.1.3 |
| @angular-devkit/schematics | 8.1.3 |
| @ngtools/webpack | 8.1.3 |
| @schematics/angular | 8.1.3 |
| @schematics/update | 0.801.3 |
| rxjs | 6.5.4 |
| typescript | 3.4.5 |
| webpack | 4.35.2 |

# Desinstalar ionic-native 
``` bash
npm uninstall @ionic-native/core @ionic-native/splash-screen @ionic-native/status-bar
```
# Modificaciones en el archivo app.module

remplazar 
``` typescript
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

```

``` typescript
providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
```
por 
``` typescript
import { Plugins, StatusBarStyle } from '@capacitor/core';
```

``` typescript
providers: [
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
```

# Modificaciones en el archivo app.component
remplezar
``` typescript
import { SplashScreen } from '@ionic-native/splash-screen/ngx';


initializeApp() {
  this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    })
 }
```
por :
```typescript
import { Plugins , StatusBarStyle } from '@capacitor/core';


async initializeApp() {
  const {SplashScreen,StatusBar} = Plugins;
    try {
      await SplashScreen.hide();
      await StatusBar.setStyle({style:StatusBarStyle.Light});
      if (this.platform.is('android')) {
        StatusBar.setBackgroundColor({color:'#CDCDCD'});
      }
    } catch (err) {
      console.log("Error normal en el navegador",err);
    }
 }

```

